#include "Customer.h"
#include <map>

#define ITEM_AMOUNT 10

#define SIGN_IN 1
#define UPDATE 2
#define EXPENSIVE_CUSTOMER 3
#define EXIT 4

#define EXIT_LIST 0
#define LAST_ITEM 10

#define ADD_ITEMS 1
#define REMOVE_ITEMS 2
#define BACK_TO_MENU 3

void mainMenu(int& choice); // printing the menu options of the shop and getting input
void loginMenu(int& choice); // printing the login options of the shop and getting input
void checkInput(int& num, const int min, const int max); // checking if input is in value range between min and max

void printItems(Item itemList[], int n); // printing the array of items for sale
void printCustomerItems(std::set<Item>& items); // printing customer's items

void signAsCustomer(std::map<std::string, Customer>& abcCustomers, Item itemList[]); // sign in and add items to list
void updateCustomer(std::map<std::string, Customer>& abcCustomers, Item itemList[]); // log in and add / remove items from list
void mostExpensiveCustomer(std::map<std::string, Customer>& abcCustomers); // print the most expensive price of a customer

void addItems(std::map<std::string, Customer>& abcCustomers, Item itemList[], std::string name); // adding items to customer's list
void removeItems(std::map<std::string, Customer>& abcCustomers, Item itemList[], std::string name); // removing items from customer's list

int main()
{
	int choice = 0;
	std::map<std::string, Customer> abcCustomers;
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };

	while (choice != EXIT) {
		mainMenu(choice);
		switch (choice)
		{
		case SIGN_IN:
			signAsCustomer(abcCustomers, itemList);
			break;
		case UPDATE:
			updateCustomer(abcCustomers, itemList);
			break;
		case EXPENSIVE_CUSTOMER:
			mostExpensiveCustomer(abcCustomers);
			break;
		}
		system("PAUSE");
		system("CLS");
	}

	return 0;
}


void mainMenu(int& choice){
	std::cout << "Welcome to MagshiMart!" << std::endl;
	std::cout << "1. to sign as customer and buy items" << std::endl;
	std::cout << "2. to uptade existing customer's items" << std::endl;
	std::cout << "3. to print the customer who pays the most" << std::endl;
	std::cout << "4. to exit" << std::endl;
	std::cin >> choice;
	(void)getchar(); // cleaning buffer
	checkInput(choice, SIGN_IN, EXIT);
}


void loginMenu(int& choice)
{
	std::cout << "1. Add items" << std::endl;
	std::cout << "2. Remove items" << std::endl;
	std::cout << "3. Back to menu" << std::endl;

	std::cout << "Enter input: " << std::endl;
	std::cin >> choice;
	(void)getchar(); // clean buffer
	checkInput(choice, ADD_ITEMS, BACK_TO_MENU);
}


void checkInput(int& num, const int min, const int max)
{
	while (num < min || num > max) {
		std::cout << "Wrong choice, enter again:" << std::endl;
		std::cin >> num;
		(void)getchar(); // clean buffer
	}
}


void printItems(Item itemList[], int n)
{
	for (int i = 0; i < n; i++) {
		std::cout << i + 1 << ". " << itemList[i].getName() << "	Price: " << itemList[i].getUnitPrice() << std::endl;
	}
}


void printCustomerItems(std::set<Item>& items)
{
	unsigned int count = 1;
	std::set<Item>::iterator it;
	for (it = items.begin(); it != items.end(); ++it, ++count)
	{
		std::cout << count << ". " << (*it).getName() << ' ' << (*it).getCount() << std::endl;
	}

	
}


void signAsCustomer(std::map<std::string, Customer>& abcCustomers, Item itemList[])
{
	std::string name = "";
	std::cout << "Enter a new name: ";
	std::getline(std::cin, name);

	if (abcCustomers.find(name) == abcCustomers.end()) { // if customer is not found
		abcCustomers[name] = Customer(name); // creating a new customer
		addItems(abcCustomers, itemList, name); // adding items to the his list (set)
	}
	else {
		std::cerr << "Error: this name already exists" << std::endl;
	}
}


void updateCustomer(std::map<std::string, Customer>& abcCustomers, Item itemList[])
{
	int choice = 0;
	std::string name = "";
	std::cout << "Enter customer's name: ";
	std::getline(std::cin, name);

	if (abcCustomers.find(name) != abcCustomers.end()) { // if customer was found

		while (choice != BACK_TO_MENU) {
			std::cout << "Your items:" << std::endl;
			printCustomerItems(abcCustomers[name].getItems()); // printing customer's items
			std::cout << std::endl;
			loginMenu(choice); // printing menu with 3 options

			if (choice == ADD_ITEMS) {
				addItems(abcCustomers, itemList, name);
			}
			else if (choice == REMOVE_ITEMS) {
				removeItems(abcCustomers, itemList, name);
			}
			else {
				continue;
			}

			system("PAUSE");
			system("CLS");
		}
	}
	else {
		std::cerr << "Error: this name doesn't exist" << std::endl;
	}
}


void mostExpensiveCustomer(std::map<std::string, Customer>& abcCustomers)
{
	std::map<std::string, Customer>::iterator itr = abcCustomers.begin();
	if (itr == abcCustomers.end()) { // if there are no customers
		std::cout << "no customers" << std::endl;
		return;
	}
	Customer customer((*itr).first); // to contain the customer with maximum price to pay

	for (itr; itr != abcCustomers.end(); ++itr) { // going threw all the customers
		if (customer.totalSum() < (*itr).second.totalSum()) {
			customer.setName((*itr).first);
			customer.setItems((*itr).second.getItems());
		}
	}

	// printing customer's data
	std::cout << customer.getName() << "	 total price: " << customer.totalSum() << std::endl;
	printCustomerItems(customer.getItems());
}


void addItems(std::map<std::string, Customer>& abcCustomers, Item itemList[], std::string name)
{
	int choice = -1;

	std::cout << "The items you can buy are: (0 to exit)" << std::endl;
	printItems(itemList, ITEM_AMOUNT); // printing available items for sale

	while (choice) {
		std::cout << "What item would you like to buy? Input: "; 
		std::cin >> choice; (void)getchar(); // getting item number

		checkInput(choice, EXIT_LIST, LAST_ITEM);
		if (choice) {
			abcCustomers[name].addItem(itemList[choice - 1]); // adding number to the list
		}
	}
}


void removeItems(std::map<std::string, Customer>& abcCustomers, Item itemList[], std::string name)
{
	int choice = -1;
	std::set<Item>::iterator it;

	std::cout << "Items to remove:" << std::endl;

	while (choice) {
		std::cout << "What item would you like to remove? (0 to exit) Input: ";
		std::cin >> choice; (void)getchar(); // getting item number to remove

		if ((abcCustomers[name].getItems()).size() == 0) { // if there are no items
			std::cout << "List is empty" << std::endl;
			break;
		}
		checkInput(choice, EXIT_LIST, (abcCustomers[name].getItems()).size());

		if (choice) {
			it = (abcCustomers[name].getItems()).begin();
			std::advance(it, choice - 1); // getting pointer to the item in the set
			abcCustomers[name].removeItem(*it); // removing item / decreasing count
			printCustomerItems(abcCustomers[name].getItems());
		}
	}
}
