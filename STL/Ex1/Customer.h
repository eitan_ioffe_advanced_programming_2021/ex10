#pragma once
#include "Item.h"
#include <set>

class Customer
{
public:
	Customer(std::string);
	Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item);//add item to the set
	void removeItem(Item);//remove item from the set

	//get and set functions
	std::string getName() const;
	std::set<Item>& getItems();
	void setName(const std::string name);
	void setItems(const std::set<Item> items);

private:
	std::string _name;
	std::set<Item> _items;

};
