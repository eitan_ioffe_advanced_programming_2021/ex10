#include "Item.h"

Item::Item(std::string name, std::string serialNumber, double unitPrice)
{
	_name = name;
	_serialNumber = serialNumber;
	_unitPrice = unitPrice;
	_count = 1;
}

Item::~Item()
{
}

double Item::totalPrice() const
{
	return _count * _unitPrice;
}

bool Item::operator<(const Item& other) const
{
	if (_serialNumber < other._serialNumber) {
		return true;
	}
	return false;
}

bool Item::operator>(const Item& other) const
{
	if (_serialNumber > other._serialNumber) {
		return true;
	}
	return false;
}

bool Item::operator==(const Item& other) const
{
	if (_serialNumber == other._serialNumber) {
		return true;
	}
	return false;
}

std::string Item::getName() const
{
	return _name;
}

std::string Item::getSerialNumber() const
{
	return _serialNumber;
}

int Item::getCount() const
{
	return _count;
}

double Item::getUnitPrice() const
{
	return _unitPrice;
}

void Item::setName(const std::string name)
{
	_name = name;
}

void Item::setSerialNumber(const std::string serialNumber)
{
	_serialNumber = serialNumber;
}

void Item::setCount(const int count)
{
	_count = count;
}

void Item::setUnitPrice(const double price)
{
	_unitPrice = price;
}
