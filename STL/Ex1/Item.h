#pragma once
#include <iostream>
#include <string>
#include <algorithm>


class Item
{
public:
	Item(std::string, std::string, double);
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.

	//get and set functions
	std::string getName() const;
	std::string getSerialNumber() const;
	int getCount() const;
	double getUnitPrice() const;

	void setName(const std::string name);
	void setSerialNumber(const std::string serialNumber);
	void setCount(const int count);
	void setUnitPrice(const double price);

private:
	std::string _name;
	std::string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!

};
