#include "Customer.h"

Customer::Customer(std::string name)
{
	_name = name;
}

Customer::Customer()
{
}

double Customer::totalSum() const
{
	double sum = 0;
	std::set<Item>::iterator itr;

	for (itr = _items.begin(); itr != _items.end(); ++itr) {
		sum += (*itr).totalPrice();
	}
	return sum;
}

void Customer::addItem(Item item)
{
	auto result = _items.insert(item);

	if (!result.second) { // if item has been inserted already
		std::set<Item>::iterator itr = _items.find(item); // getting a pointer to item in the set

		// creating a new item with an updated counter (no copy constructor :()
		Item newItem((*itr).getName(), (*itr).getSerialNumber(), (*itr).getUnitPrice());
		newItem.setCount((*itr).getCount() + 1);

		// erasing previous item and inserting a new one
		_items.erase(result.first);
		_items.insert(newItem);
	}
}

void Customer::removeItem(Item item)
{
	std::set<Item>::iterator itr = _items.find(item); // getting the pointer to the item to remove

	if ((*itr).getCount() > 1) {
		// creating a new item with decreased count
		Item newItem((*itr).getName(), (*itr).getSerialNumber(), (*itr).getUnitPrice());
		newItem.setCount((*itr).getCount() - 1);

		// erasing previous item and inserting a new one
		_items.erase(item);
		_items.insert(newItem);
	}
	else {
		_items.erase(item); // erasing item completely from items
	}

}

std::string Customer::getName() const
{
	return _name;
}

std::set<Item>& Customer::getItems()
{
	return _items;
}

void Customer::setName(const std::string name)
{
	_name = name;
}

void Customer::setItems(const std::set<Item> items)
{
	_items = items;
}
